import './styles/index.scss'

// window.Vue = require('vue');
// let Vue = window.Vue;
import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter);
import store from './store'

import {
    ButtonPlugin,
    CollapsePlugin,
    FormPlugin,
    FormGroupPlugin,
    FormInputPlugin,
    FormSelectPlugin,
    FormTextareaPlugin,
    InputGroupPlugin,
    LayoutPlugin,
    LinkPlugin,
    NavPlugin,
    NavbarPlugin,
    ModalPlugin,
} from 'bootstrap-vue'

Vue.use(ButtonPlugin)
Vue.use(CollapsePlugin)
Vue.use(FormPlugin)
Vue.use(FormGroupPlugin)
Vue.use(FormInputPlugin)
Vue.use(FormSelectPlugin)
Vue.use(FormTextareaPlugin)
Vue.use(InputGroupPlugin)
Vue.use(LayoutPlugin)
Vue.use(LinkPlugin)
Vue.use(NavPlugin)
Vue.use(NavbarPlugin)
Vue.use(ModalPlugin)

Vue.component('header-component', require('./components/HeaderComponent.vue').default)
Vue.component('main-slider-component', require('./components/MainSliderComponent.vue').default)
Vue.component('footer-component', require('./components/FooterComponent.vue').default)
Vue.component('contact-form-component', require('./components/ContactFormComponent.vue').default)
Vue.component('slider', require('./components/Slider.vue').default)
Vue.component('vue-anchor-router-link', require('../node_modules/vue-anchor-router-link/src/components/VueAnchorRouterLink.vue').default)

Vue.mixin({
    computed: {
        console: () => console
    }
})

const routes = [
    { path: '/contact', name: 'contact', component: require('./components/ContactPage.vue').default },
    { path: '/privacy', name: 'privacy-policy', component: require('./components/PrivacyPolicyPage.vue').default },
    { path: '', name: 'home', component: require('./components/MainPage.vue').default },
    { path: '*', name: 'not-found', component: require('./components/NotFoundPage.vue').default }
]

const router = new VueRouter({
    mode: 'history',
    base: '/',
    routes,
    scrollBehavior (to, from, savedPosition) {
        return { x: 0, y: 0 }
    }
})

const app = new Vue({
    router,
    store,
    el: '#app',

    mounted() {
        setTimeout(() => this.scrollFix(this.$route.hash), 1);
    },

    methods: {
        scrollFix(hashbang) {
            location.hash = hashbang;
        }
    }
})
