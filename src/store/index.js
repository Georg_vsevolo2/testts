import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        apps: [
            {
                title: 'Widgetter',
                subtitle: 'Custom Deskto‪p',
                text: 'Widgetter is a direct line to upscaling your desktop into a work station perfectly tailored to your needs. Start building a customized home screen to have all the key data points and basic functions always remain available at a glance.',
                links: [
                    {type: 'appstore', url: 'https://apps.apple.com/app/id1553223588?mt=12'}
                ],
                items: [
                    {
                        src: '/assets/images/widgetter/promo/en/01.png',
                        w: 1440,
                        h: 900,
                        title: ''
                    },
                    {
                        src: '/assets/images/widgetter/promo/en/02.png',
                        w: 1440,
                        h: 900,
                        title: ''
                    },
                    {
                        src: '/assets/images/widgetter/promo/en/03.png',
                        w: 1440,
                        h: 900,
                        title: ''
                    },
                    {
                        src: '/assets/images/widgetter/promo/en/04.png',
                        w: 1440,
                        h: 900,
                        title: ''
                    },
                    {
                        src: '/assets/images/widgetter/promo/en/05.png',
                        w: 1440,
                        h: 900,
                        title: ''
                    }
                ],
                images: [
                    '/assets/images/widgetter/promo/en/01.png',
                    '/assets/images/widgetter/promo/en/02.png',
                    '/assets/images/widgetter/promo/en/03.png',
                    '/assets/images/widgetter/promo/en/04.png',
                    '/assets/images/widgetter/promo/en/05.png'
                ],
                bgImage: '/assets/images/widgetter/bg.png',
                icon: '/assets/images/widgetter/icon.svg'
            },
            {
                title: 'Book Night',
                subtitle: 'Pocket Librar‪y',
                text: 'Book Night is a laconic and intuitive tool for a perfect e-reading experience. Without extra fluff or complexity, it takes care of all the basic needs: managing your collection, getting the text on the screen and letting you interact with it as you go.',
                links: [
                    {type: 'appstore', url: 'https://apps.apple.com/app/id1550557954'}
                ],
                items: [
                    {
                        src: '/assets/images/book-night/promo/en/01.png',
                        w: 621,
                        h: 1344,
                        title: ''
                    },
                    {
                        src: '/assets/images/book-night/promo/en/02.png',
                        w: 621,
                        h: 1344,
                        title: ''
                    },
                    {
                        src: '/assets/images/book-night/promo/en/03.png',
                        w: 621,
                        h: 1344,
                        title: ''
                    },
                    {
                        src: '/assets/images/book-night/promo/en/04.png',
                        w: 621,
                        h: 1344,
                        title: ''
                    }
                ],
                images: [
                    '/assets/images/book-night/promo/en/01.png',
                    '/assets/images/book-night/promo/en/02.png',
                    '/assets/images/book-night/promo/en/03.png',
                    '/assets/images/book-night/promo/en/04.png'
                ],
                bgImage: '/assets/images/book-night/bg.png',
                icon: '/assets/images/book-night/icon.svg'
            },
        ],
        countries: ["Andorra", "United Arab Emirates", "Afghanistan", "Antigua and Barbuda", "Anguilla", "Albania", "Armenia", "Angola", "Antarctica", "Argentina", "American Samoa", "Austria", "Australia", "Aruba", "Åland Islands", "Azerbaijan", "Bosnia and Herzegovina", "Barbados", "Bangladesh", "Belgium", "Burkina Faso", "Bulgaria", "Bahrain", "Burundi", "Benin", "Saint Barthélemy", "Bermuda", "Brunei Darussalam", "Bolivia, Plurinational State of", "Bonaire, Sint Eustatius and Saba", "Brazil", "Bahamas", "Bhutan", "Bouvet Island", "Botswana", "Belarus", "Belize", "Canada", "Cocos (Keeling) Islands", "Congo, Democratic Republic of the", "Central African Republic", "Congo", "Switzerland", "Côte d'Ivoire", "Cook Islands", "Chile", "Cameroon", "China", "Colombia", "Costa Rica", "Cuba", "Cabo Verde", "Curaçao", "Christmas Island", "Cyprus", "Czechia", "Germany", "Djibouti", "Denmark", "Dominica", "Dominican Republic", "Algeria", "Ecuador", "Estonia", "Egypt", "Western Sahara", "Eritrea", "Spain", "Ethiopia", "Finland", "Fiji", "Falkland Islands (Malvinas)", "Micronesia, Federated States of", "Faroe Islands", "France", "Gabon", "United Kingdom of Great Britain and Northern Ireland", "Grenada", "Georgia", "French Guiana", "Guernsey", "Ghana", "Gibraltar", "Greenland", "Gambia", "Guinea", "Guadeloupe", "Equatorial Guinea", "Greece", "South Georgia and the South Sandwich Islands", "Guatemala", "Guam", "Guinea-Bissau", "Guyana", "Hong Kong", "Heard Island and McDonald Islands", "Honduras", "Croatia", "Haiti", "Hungary", "Indonesia", "Ireland", "Israel", "Isle of Man", "India", "British Indian Ocean Territory", "Iraq", "Iran, Islamic Republic of", "Iceland", "Italy", "Jersey", "Jamaica", "Jordan", "Japan", "Kenya", "Kyrgyzstan", "Cambodia", "Kiribati", "Comoros", "Saint Kitts and Nevis", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Cayman Islands", "Kazakhstan", "Lao People's Democratic Republic", "Lebanon", "Saint Lucia", "Liechtenstein", "Sri Lanka", "Liberia", "Lesotho", "Lithuania", "Luxembourg", "Latvia", "Libya", "Morocco", "Monaco", "Moldova, Republic of", "Montenegro", "Saint Martin, (French part)", "Madagascar", "Marshall Islands", "North Macedonia", "Mali", "Myanmar", "Mongolia", "Macao", "Northern Mariana Islands", "Martinique", "Mauritania", "Montserrat", "Malta", "Mauritius", "Maldives", "Malawi", "Mexico", "Malaysia", "Mozambique", "Namibia", "New Caledonia", "Niger", "Norfolk Island", "Nigeria", "Nicaragua", "Netherlands", "Norway", "Nepal", "Nauru", "Niue", "New Zealand", "Oman", "Panama", "Peru", "French Polynesia", "Papua New Guinea", "Philippines", "Pakistan", "Poland", "Saint Pierre and Miquelon", "Pitcairn", "Puerto Rico", "Palestine, State of", "Portugal", "Palau", "Paraguay", "Qatar", "Réunion", "Romania", "Serbia", "Russian Federation", "Rwanda", "Saudi Arabia", "Solomon Islands", "Seychelles", "Sudan", "Sweden", "Singapore", "Saint Helena, Ascension and Tristan da Cunha", "Slovenia", "Svalbard and Jan Mayen", "Slovakia", "Sierra Leone", "San Marino", "Senegal", "Somalia", "Suriname", "South Sudan", "Sao Tome and Principe", "El Salvador", "Sint Maarten, (Dutch part)", "Syrian Arab Republic", "Eswatini", "Turks and Caicos Islands", "Chad", "French Southern Territories", "Togo", "Thailand", "Tajikistan", "Tokelau", "Timor-Leste", "Turkmenistan", "Tunisia", "Tonga", "Turkey", "Trinidad and Tobago", "Tuvalu", "Taiwan, Province of China", "Tanzania, United Republic of", "Ukraine", "Uganda", "United States Minor Outlying Islands", "United States of America", "Uruguay", "Uzbekistan", "Holy See", "Saint Vincent and the Grenadines", "Venezuela, Bolivarian Republic of", "Virgin Islands, British", "Virgin Islands, U.S.", "Viet Nam", "Vanuatu", "Wallis and Futuna", "Samoa", "Yemen", "Mayotte", "South Africa", "Zambia", "Zimbabwe"],
        requestTypes: [
            {value: 'general', text: 'General issues'},
            {value: 'apps', text: 'Apps'}
        ],
        languages: [
            {code: 'en', name: 'English'},
            {code: 'ru', name: 'Русский'},
            {code: 'cn', name: '简体中文'},
            {code: 'zh', name: '繁體中文'}
        ],
        socialLinks: [
            {href: 'https://apps.apple.com/us/developer/villow/id1550558286', icon: 'apple'},
            {href: '/contact', icon: 'mail'},
            {href: '#', icon: 'facebook'},
            {href: '#', icon: 'linkedin'},
            {href: '#', icon: 'android'},
            {href: '#', icon: 'googleplay'},
            {href: '#', icon: 'windows'}
        ],
        copyright: '© <strong>2021 Villow, LLC.</strong> All rights reserved.',
        company: {
            name: 'ООО «Рога и копыта»',
            ogrn: '123456789',
            inn: '123456789'
        }
    },
    mutations: {
    },
    actions: {
    },
    getters: {
    }
})
